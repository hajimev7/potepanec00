# frozen_string_literal: true

require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'product page' do
    let(:product) { create :product }

    before do
      get :show, params: { id: product.id }
    end

    it '正しいレスポンスが戻ってくる ' do
      expect(response).to be_successful
    end

    it 'HTTPステータスが200であること' do
      expect(response).to have_http_status '200'
    end

    it '正しいインスタンス変数がセットされていること' do
      expect(assigns(:product)).to eq product
    end

    it '正しいビューテンプレートが返却されてること' do
      expect(response).to render_template(:show)
    end
  end
end
