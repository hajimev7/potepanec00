# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'カテゴリーページページ表示' do
    let!(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:taxonomy) { create(:taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:other_product) { create(:product) }

    before do
      get :show, params: { id: taxon.id }
    end

    it '@taxonが期待される値をもつ' do
      expect(assigns(:taxon)).to eq taxon
    end

    it '@taxonomiesが期待される値をもつ' do
      expect(assigns(:taxonomies)).to contain_exactly taxonomy
    end

    it '@productsが期待される値をもつ' do
      expect(assigns(:products)).to contain_exactly product
    end

    it 'レスポンス成功' do
      expect(response).to be_successful
    end

    it 'showページが表示される' do
      expect(response).to render_template :show
    end
  end
end
