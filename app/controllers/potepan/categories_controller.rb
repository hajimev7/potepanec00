# frozen_string_literal: true

class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.all.includes(:root)
    @products = @taxon.all_products.includes(master: %i[default_price images])
  end
end
